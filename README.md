# The Colonists

Attempt at Oxygen Not Included in Godot 3.5.2

The Colonists is a survival simulation game. At the start of a new game, three colonists find themselves in the shallow crust of a procedurally generated planetoid. With the need to grow and expand the colony (or at least keep the current alive), the player is tasked with taking care of these colonists and to create a sustainable makeshift space colony. The player must monitor the colonists' hunger, happiness and oxygen levels to keep them alive.

Each planetoid is procedurally generated. The planetoid is subdivided into different possible biomes, each with their own specific quirk, materials and creatures.

To help establish the colony, the player directs the colonists to perform certain tasks, such as mining for resources, growing food, crafting equipment, building structures, researching new technologies and maintaining their own health through nourishment, rest, and hygiene. The player does not control the colonists directly, and instead provides prioritized instructions, from which the colonists will then follow to the best of their abilities. If a task cannot be completed (missing materials, no path, etc), it will be left uncompleted and the colonist will go to complete other tasks they can do.

Ripped most of this off of wikipedia and adjusted it for my vision. In the future, the player may choose to start deep inside the planetoid, similar to how Oxygen Not Included does so.

The reason I choose to clone all of these popular and modern games is so I can gain experience and have a set direction of what to craft. I also do not like proprietary video games but wish to play all of my favorites. Open source games tend to feel very outdated and for a specific audience. I want to give FOSS players a greater choice in their entertainment and share that Godot can be a great engine to work with.

I cannot write well in C# so GDscript will have to do for now. This means any actual fluid and gas particle simulations would be very slow and unplayable. I will just try having a 3D camera facing in a 2D way, then have spheres with a 3D sprite texture or water shader (which makes them clump up) roll around for liquid. Reverse the gravity (maybe via script) for gases. The colonists will be 3D collision objects with a 3D sprite on the side. Ground and terrain will have detections for when to fall (figure out later).
